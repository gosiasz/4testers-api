import requests


class PokeAPIHandler:
    base_url = "https://pokeapi.co/api/v2"
    pokemon_endpoint = "/pokemon"
    pokemon_shape_endpoint = "/pokemon-shape"

    def get_list_of_pokemons(self, params=None):
        response = requests.get(self.base_url + self.pokemon_endpoint, params=params)
        assert response.status_code == 200
        return response

    def get_shapes_of_pokemons(self, name=""):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint + f"/{name}")
        assert response.status_code == 200
        return response
