import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer efcd61d6e5c2fb714f17f54092f23d3332299150dad49942cabfd6ca089e2249"
    }

    def create_user(self, user_data, existing=False):
        response = requests.post(self.base_url + self.users_endpoint, headers=self.headers, json=user_data)
        if not existing:
            assert response.status_code == 201
        else:
            assert response.status_code == 422
        return response

    def get_user(self, user_id, existing=True):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        if existing:
            assert response.status_code == 200
        else:
            assert response.status_code == 404
        return response

    def update_user(self, user_id, user_data):
        response = requests.patch(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers, json=user_data)
        assert response.status_code == 200
        return response

    def delete_user(self, user_id):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 204
